# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 20:51:41 2019

@author: Administrator
"""

import torch
import torch.nn.functional as F
import torch.nn as nn
from utils.data_utils import *
from utils.core_nns import *
import torch.optim as optim
import time
import math
device=torch.device('cuda' if torch.cuda.is_available else 'cpu')
import random
class Seq2seq_model(object):
    def __init__(self,encoder,decoder):
        self.encoder=encoder
        self.decoder=decoder
        self.lr=0.01
        self.encoder_optimizer=optim.SGD(self.encoder.parameters(),self.lr)
        self.decoder_optimizer=optim.SGD(self.decoder.parameters(),self.lr)
        self.max_length=10
        self.device=device=torch.device('cuda' if torch.cuda.is_available else 'cpu')
        
    def train(self,input_tensor,target_tensor,criterion,teacher_forcing_ratio=0.5):
        encoder_hidden=self.encoder.initHidden()
        
        self.encoder_optimizer.zero_grad()
        self.decoder_optimizer.zero_grad()
        
        input_length=input_tensor.size(0)
        target_length=target_tensor.size(0)
        
        encoder_outputs=torch.zeros(self.max_length,self.encoder.hidden_size,device=self.device)
        loss=0
        
        for ei in range(input_length):
            encoder_output,encoder_hidden=self.encoder(input_tensor[ei],encoder_hidden)
            encoder_outputs[ei]=encoder_output[0,0]
            
        decoder_input=torch.tensor([[SOS_token]], device=device)
        decoder_hidden=encoder_hidden
            
        use_teacher_forcing=True if random.random() < teacher_forcing_ratio else False
        if use_teacher_forcing:
            for di in range(target_length):
                decoder_output,decoder_hidden=self.decoder(decoder_input,decoder_hidden)
                loss += criterion(decoder_output, target_tensor[di])
                decoder_input = target_tensor[di]  # Teacher forcing
        else:
            for di in range(target_length):
                decoder_output,decoer_hidden=self.decoder(decoder_input,decoder_hidden)
                topv, topi = decoder_output.topk(1)
                decoder_input = topi.squeeze().detach()  # detach from history as input

                loss += criterion(decoder_output, target_tensor[di])
                if decoder_input.item() == EOS_token:
                    break

        loss.backward()

        self.encoder_optimizer.step()
        self.decoder_optimizer.step()

        return loss.item() / target_length
    
    def asMinutes(s):
        m = math.floor(s / 60)
        s -= m * 60
        return '%dm %ds' % (m, s)


    def timeSince(since, percent):
        now = time.time()
        s = now - since
        es = s / (percent)
        rs = es - s
        return '%s (- %s)' % (Seq2seq_model.asMinutes(s), Seq2seq_model.asMinutes(rs))
    
    def trainIters(self,fname,n_iters,print_every=1000):
        start = time.time()
        print_loss_total=0
        input_lang, output_lang, pairs = preprocess.read_file(fname)
        training_pairs = [Data2tensor.tensorsFromPair(input_lang,output_lang,random.choice(pairs))
                          for i in range(n_iters)]
        criterion = nn.NLLLoss()
        for iter in range(1, n_iters + 1):
            training_pair = training_pairs[iter - 1]
            input_tensor = training_pair[0]
            target_tensor = training_pair[1]

            loss = Seq2seq_model.train(self,input_tensor, target_tensor,criterion)
            print_loss_total += loss

            if iter % print_every == 0:
                print_loss_avg = print_loss_total / print_every
                print_loss_total = 0
                print('%s (%d %d%%) %.4f' % (Seq2seq_model.timeSince(start, iter / n_iters),
                                             iter, iter / n_iters * 100, print_loss_avg))
    
    def evaluate(self, sentence,fname):
        with torch.no_grad():
            input_lang, output_lang, pairs = preprocess.read_file(fname)
            input_tensor = Data2tensor.tensorFromSentence(input_lang, sentence)
            input_length = input_tensor.size()[0]
            encoder_hidden = self.encoder.initHidden()

            encoder_outputs = torch.zeros(self.max_length, self.encoder.hidden_size, device=self.device)

            for ei in range(input_length):
                encoder_output, encoder_hidden = self.encoder(input_tensor[ei],
                                                     encoder_hidden)
                encoder_outputs[ei] += encoder_output[0, 0]

            decoder_input = torch.tensor([[SOS_token]], device=self.device)  # SOS

            decoder_hidden = encoder_hidden

            decoded_words = []

            for di in range(self.max_length):
                decoder_output, decoder_hidden= self.decoder(decoder_input, decoder_hidden)
                topv, topi = decoder_output.data.topk(1)
                if topi.item() == EOS_token:
                    decoded_words.append('<EOS>')
                    break
                else:
                    decoded_words.append(output_lang.index2word[topi.item()])

                decoder_input = topi.squeeze().detach()

            return decoded_words
    def evaluateRandomly(self, fname,n=10):
        input_lang, output_lang, pairs = preprocess.read_file(fname)
        for i in range(n):
            pair = random.choice(pairs)
            print('>', pair[0])
            print('=', pair[1])
            output_words = Seq2seq_model.evaluate(self, pair[0],fname)
            output_sentence = ' '.join(output_words)
            print('<', output_sentence)
            print('')