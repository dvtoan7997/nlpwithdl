# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 09:37:46 2019

@author: Administrator
"""

from __future__ import unicode_literals, print_function, division
import torch
import unicodedata
from io import open
import re

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
SOS_token = 0
EOS_token = 1

class preprocess(object):
    def unicodeToAscii(s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn')
    def normalizeString(s):
        s = preprocess.unicodeToAscii(s.lower().strip())
        s = re.sub(r"([.!?])", r" \1", s)
        s = re.sub(r"[^a-zA-Z.!?]+", r" ", s)
        return s
    def read_file(fname):
        MAX_LENGTH=10
        lines=open(fname,encoding='utf-8').read().strip().split('\n')
        pairs_fake=[[preprocess.normalizeString(s) for s in line.split('\t')] for line in lines]
        source=Lang()
        target=Lang()
        pairs=[]
        for p in pairs_fake:
            if len(p[0].split(' ')) < MAX_LENGTH and len(p[1].split(' '))< MAX_LENGTH:
                full=[p[0],p[1]]
                source.addSentence(p[0])
                target.addSentence(p[1])
                pairs.append(full)
        return source,target,pairs

class Lang(object):
    def __init__(self):
        self.word2index={}
        self.word2count={}
        self.index2word={0:'SOS',1:'EOS'}
        self.n_words=2
    def addSentence(self,sentence):
        for word in sentence.split(' '):
            self.addWord(word)
    def addWord(self,word):
        if word not in self.word2index:
            self.word2index[word]=self.n_words
            self.word2count[word]=1
            self.index2word[self.n_words]=word
            self.n_words+=1
        else:
            self.word2count[word]+=1

class Data2tensor(object):
    def indexesFromSentence(lang,sentence):
        return [lang.word2index[word] for word in sentence.split(' ')]
    def tensorFromSentence(lang,sentence):
        indexes=Data2tensor.indexesFromSentence(lang,sentence)
        indexes.append(EOS_token)
        return torch.tensor(indexes,dtype=torch.long, device=device).view(-1, 1)
    def tensorsFromPair(input_lang,output_lang,pair):
        input_tensor=Data2tensor.tensorFromSentence(input_lang,pair[0])
        output_tensor=Data2tensor.tensorFromSentence(output_lang,pair[1])
        return input_tensor, output_tensor